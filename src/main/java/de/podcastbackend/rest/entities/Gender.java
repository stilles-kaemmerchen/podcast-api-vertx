package de.podcastbackend.rest.entities;

import io.vertx.core.json.JsonObject;

public class Gender {

    private final String name;

    public Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("name", name);

        return jsonObject;
    }
}
