package de.podcastbackend.rest.entities;

import io.vertx.core.json.JsonObject;

public class Format {

    private Integer formatId;
    private final String name;
    private final String description;
    private final String logo;

    public Format(String name, String description, String logo) {
        this.name = name;
        this.description = description;
        this.logo = logo;
    }

    public Integer getFormatId() {
        return formatId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLogo() {
        return logo;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("name", name);
        jsonObject.put("description", description);
        jsonObject.put("logo", logo);

        return jsonObject;
    }
}
