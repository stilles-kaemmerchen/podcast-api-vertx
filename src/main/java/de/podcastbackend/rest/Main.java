package de.podcastbackend.rest;

import de.podcastbackend.rest.verticles.FormatVerticle;
import de.podcastbackend.rest.verticles.GenderVerticle;
import de.podcastbackend.rest.verticles.MainVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final Vertx VERTX = Vertx.vertx();

    public static void main(String[] args) {
        startUp(deployment -> {
            if (deployment.failed()) {
                deployment.cause().printStackTrace();
            }
        });
    }

    private static void startUp(Handler<AsyncResult<CompositeFuture>> callback) {
        System.out.println("Initializing service");

        final Router router = Router.router(VERTX);

        router.route().handler(BodyHandler.create());

        Future genderFuture = Future.future();
        Future formatFuture = Future.future();
        Future mainFuture = Future.future();

        List<Future> futureList = new ArrayList();
        futureList.add(genderFuture);
        futureList.add(formatFuture);
        futureList.add(mainFuture);

        CompositeFuture.all(futureList).setHandler(callback);

        VERTX.deployVerticle(new GenderVerticle(router),
            (deployment) -> handleDeployment(genderFuture, deployment)
        );
        VERTX.deployVerticle(new FormatVerticle(router),
            (deployment) -> handleDeployment(formatFuture, deployment)
        );
        VERTX.deployVerticle(new MainVerticle(router),
            (deployment) -> handleDeployment(mainFuture, deployment)
        );
    }

    private static void handleDeployment(Future future, AsyncResult<String> result){
        if(result.succeeded()) {
            future.complete();
        } else {
            future.fail(result.cause());
        }
    }
}
