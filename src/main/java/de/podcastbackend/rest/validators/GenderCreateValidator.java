package de.podcastbackend.rest.validators;

import de.podcastbackend.rest.commands.gender.GenderCreateCommand;
import de.podcastbackend.rest.entities.Gender;
import io.vavr.control.Validation;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class GenderCreateValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenderCreateValidator.class);
    public static final String EMPTY_NAME = "No name was given";

    public static Validation<String, Gender> validate(GenderCreateCommand genderCreateCommand) {
        return validateGenderName(genderCreateCommand);
    }

    private static Validation<String, Gender> validateGenderName(GenderCreateCommand genderCreateCommand) {
        final String name = genderCreateCommand.getName();
        LOGGER.debug("validateGenderName -> genderName {}", name);
        if(name.isEmpty()) {
            LOGGER.debug("validateGenderName -> genderName is empty");
            return Validation.invalid(EMPTY_NAME);
        } else if(name.length() == 0) {
            LOGGER.debug("validateGenderName -> genderName is empty");
            return Validation.invalid(EMPTY_NAME);
        }

        Gender gender = new Gender(genderCreateCommand.getName());

        return Validation.valid(gender);
    }
}
