package de.podcastbackend.rest.verticles;

import de.podcastbackend.rest.handler.gender.GenderCreateHandler;
import de.podcastbackend.rest.handler.gender.GenderGetHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;

public class GenderVerticle extends AbstractVerticle  {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenderVerticle.class);

    private final Router router;

    public GenderVerticle(Router router) {
        this.router = router;
    }

    @Override
    public void start() throws Exception {
        super.start();

        LOGGER.info("Deploying GenderVerticle");
        router.get("/gender/:name").handler(new GenderGetHandler());
        router.post("/gender").handler(new GenderCreateHandler());
    }
}
