package de.podcastbackend.rest.verticles;

import de.podcastbackend.rest.handler.format.FormatCreateHandler;
import de.podcastbackend.rest.handler.format.FormatGetHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;

public class FormatVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(FormatVerticle.class);

    private final Router router;

    public FormatVerticle(Router router) {
        this.router = router;
    }

    @Override
    public void start() throws Exception {
        super.start();

        LOGGER.info("Deploying FormatVerticle");
        router.get("/format/:name").handler(new FormatGetHandler());
        router.post("/format").handler(new FormatCreateHandler());
    }
}
