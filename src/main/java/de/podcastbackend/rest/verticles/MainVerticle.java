package de.podcastbackend.rest.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);
    private final Router router;

    public MainVerticle(final Router router) {
        this.router = router;
    }

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
    }

    @Override
    public void start(final Future<Void> future) throws Exception {
        createHttpServer(server -> {
            if (server.succeeded()) {
                final int actualPort = server.result().actualPort();
                System.out.println("listening on port " + actualPort);
                LOGGER.info("Listening on port " + actualPort);

                future.complete();
            } else {
                future.fail(server.cause());
            }
        });
    }

    private void createHttpServer(final Handler<AsyncResult<HttpServer>> handler) {
        getVertx().createHttpServer()
            .requestHandler(router::accept)
            .listen(8080, handler);
    }
}
