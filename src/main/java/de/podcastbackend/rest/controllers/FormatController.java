package de.podcastbackend.rest.controllers;

import de.podcastbackend.rest.commands.format.FormatCreateCommand;
import de.podcastbackend.rest.commands.format.FormatGetCommand;
import de.podcastbackend.rest.entities.Format;
import de.podcastbackend.rest.exceptions.FormatFoundException;
import de.podcastbackend.rest.exceptions.FormatNotFoundException;
import io.vavr.control.Either;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

public class FormatController {

    private static FormatController instance;
    public static final Logger LOGGER = LoggerFactory.getLogger(FormatController.class);

    public static FormatController getInstance() {
        if(instance == null) {
            instance = new FormatController();
        }

        return instance;
    }

    private Map<String, Format> formats = new HashMap<>();

    public Either<Throwable, Format> create(FormatCreateCommand formatCreateCommand) {
        if(false) {
            LOGGER.warn("formatCrateCommand validation failed");
            return Either.left(new Exception(""));
        }

        LOGGER.info("got valid formatCreateCommand");

        if(formats.keySet().contains(formatCreateCommand.getName())) {
            return Either.left(new FormatFoundException(
                String.format("Format with the name \"%s\" already in the Database", formatCreateCommand.getName())
            ));
        }

        Format format = new Format(
            formatCreateCommand.getName(),
            formatCreateCommand.getDescription(),
            formatCreateCommand.getLogo()
        );

        formats.put(formatCreateCommand.getName(), format);

        return Either.right(format);
    }

    public Either<Throwable, Format> get(FormatGetCommand formatGetCommand) {
        if(formats.keySet().contains(formatGetCommand.getName())) {
            return Either.right(formats.get(formatGetCommand.getName()));
        }

        return Either.left(new FormatNotFoundException(
            String.format("Format with the name \"%s\" could not be found", formatGetCommand.getName())
        ));
    }
}
