package de.podcastbackend.rest.controllers;

import de.podcastbackend.rest.commands.gender.GenderCreateCommand;
import de.podcastbackend.rest.commands.gender.GenderGetCommand;
import de.podcastbackend.rest.entities.Gender;
import de.podcastbackend.rest.exceptions.GenderFoundException;
import de.podcastbackend.rest.exceptions.GenderNotFoundException;
import de.podcastbackend.rest.validators.GenderCreateValidator;
import io.vavr.control.Either;
import io.vavr.control.Validation;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

public class GenderController {

    private static GenderController instance;
    private static final Logger LOGGER = LoggerFactory.getLogger(GenderController.class);

    private Map<String, Gender> genders = new HashMap();


    public static GenderController getInstance() {
        if(instance == null) {
            instance = new GenderController();
        }

        return instance;
    }

    public Either<Throwable, Object> create(GenderCreateCommand genderCreateCommand) {
        Validation<String, Gender> validate = GenderCreateValidator.validate(genderCreateCommand);

        if(validate.isInvalid()) {
            LOGGER.warn("createGenderCommand validation failed");
            return Either.left(new Exception(validate.getError()));
        }

        LOGGER.info("got valid createGenderCommand");

        if(genders.keySet().contains(validate.get().getName())) {
          return Either.left(new GenderFoundException(
                String.format("Gender with the name \"%s\" already in the Database", validate.get().getName())
              )
          );
        }

        genders.put(validate.get().getName(), validate.get());

        return Either.right(validate.get());
    }

    public Either<Throwable, Gender> get(GenderGetCommand genderGetCommand) {
        if(genders.keySet().contains(genderGetCommand.getName())) {
            return Either.right(genders.get(genderGetCommand.getName()));
        }

        return Either.left(new GenderNotFoundException(
            String.format("Gender with the name \"%s\" could not be found", genderGetCommand.getName())
            )
        );
    }
}
