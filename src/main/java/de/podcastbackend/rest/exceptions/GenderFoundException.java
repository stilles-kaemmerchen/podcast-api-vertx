package de.podcastbackend.rest.exceptions;

public class GenderFoundException extends Exception {

    public GenderFoundException(String message) {
        super(message);
    }
}
