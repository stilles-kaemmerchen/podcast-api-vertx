package de.podcastbackend.rest.exceptions;

public class GenderNotFoundException extends Exception {
    public GenderNotFoundException(String message) {
        super(message);
    }
}
