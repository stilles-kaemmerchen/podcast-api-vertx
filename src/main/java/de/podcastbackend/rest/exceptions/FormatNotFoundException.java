package de.podcastbackend.rest.exceptions;

public class FormatNotFoundException extends Exception{
    public FormatNotFoundException(String message) {
        super(message);
    }
}
