package de.podcastbackend.rest.exceptions;

public class FormatFoundException extends Exception {
    public FormatFoundException(String message) {
        super(message);
    }
}
