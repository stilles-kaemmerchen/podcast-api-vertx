package de.podcastbackend.rest.commands.format;

public class FormatGetCommand {

    private final String name;

    public FormatGetCommand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
