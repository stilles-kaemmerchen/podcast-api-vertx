package de.podcastbackend.rest.commands.format;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.JsonObject;

public class FormatCreateCommand {

    private int formatId;
    private final String name;
    private final String description;
    private final String logo;

    public FormatCreateCommand(
        @JsonProperty(required = true, value = "name") String name,
        @JsonProperty(required = true, value = "description") String description,
        @JsonProperty(required = true, value = "logo") String logo
    ) {
        this.name = name;
        this.description = description;
        this.logo = logo;
    }

    public int getFormatId() {
        return formatId;
    }

    public void setFormatId(int formatId) {
        this.formatId = formatId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLogo() {
        return logo;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("name", name);
        jsonObject.put("description", description);
        jsonObject.put("logo", logo);

        return jsonObject;
    }
}
