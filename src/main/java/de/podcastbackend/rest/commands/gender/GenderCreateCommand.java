package de.podcastbackend.rest.commands.gender;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.JsonObject;

public class GenderCreateCommand {

    private int genderId;
    private String name;

    @JsonCreator
    public GenderCreateCommand(
        @JsonProperty(required = true, value = "name") String name
    ) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("name", name);

        return jsonObject;
    }
}
