package de.podcastbackend.rest.commands.gender;

import io.vertx.core.json.JsonObject;

public class GenderGetCommand {

    private final String name;

    public GenderGetCommand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("name", name);

        return jsonObject;
    }
}
