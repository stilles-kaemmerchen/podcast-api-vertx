package de.podcastbackend.rest.handler.format;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_CREATED;

import de.podcastbackend.rest.commands.format.FormatCreateCommand;
import de.podcastbackend.rest.controllers.FormatController;
import de.podcastbackend.rest.entities.Format;
import de.podcastbackend.rest.exceptions.FormatFoundException;
import io.vavr.control.Either;
import io.vavr.control.Try;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

public class FormatCreateHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FormatCreateHandler.class);

    private final FormatController formatController = FormatController.getInstance();

    @Override
    public void handle(RoutingContext rCtx) {
        LOGGER.info("handle post on {}", rCtx.request().params());

        Either<Throwable, FormatCreateCommand> formatCreateCommand =
            decodeFormatCreateCommand(rCtx.getBodyAsString());

        if(formatCreateCommand.isLeft()) {
            handleBadRequest(rCtx, formatCreateCommand.getLeft());
        } else {
            createFormatAndStore(rCtx, formatCreateCommand.get());
        }
    }

    private void createFormatAndStore(RoutingContext rCtx, FormatCreateCommand formatCreateCommand) {
        Either.<Throwable, FormatCreateCommand>right(formatCreateCommand)
            .flatMap(formatController::create)
            .peekLeft(exception -> handleBadRequest(rCtx, exception))
            .peek(result -> handleSuccessfullyCreation(rCtx, result));
    }

    private void handleSuccessfullyCreation(RoutingContext rCtx, Format format) {
        LOGGER.info("successfully create format");
        rCtx
            .response()
            .putHeader("Location", "/format/" + format.getName())
            .setStatusCode(HTTP_CREATED)
            .end(format.toJsonObject().toString());
    }

    private void handleBadRequest(RoutingContext rCtx, Throwable extension) {
        int status = HTTP_BAD_REQUEST;
        String message = "";

        if(extension instanceof FormatFoundException) {
            status = HTTP_CONFLICT;
            message = extension.getMessage();
        }
        rCtx.response()
            .setStatusCode(status)
            .end(message);
    }

    private Either<Throwable, FormatCreateCommand> decodeFormatCreateCommand(String json) {
        LOGGER.info(String.format("JSON body %s", json));
        return Try.of(() -> Json.decodeValue(json, FormatCreateCommand.class)).toEither();
    }
}
