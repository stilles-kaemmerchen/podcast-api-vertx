package de.podcastbackend.rest.handler.format;

import de.podcastbackend.rest.commands.format.FormatGetCommand;
import de.podcastbackend.rest.controllers.FormatController;
import de.podcastbackend.rest.entities.Format;
import de.podcastbackend.rest.exceptions.FormatNotFoundException;
import io.vavr.control.Either;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import java.net.HttpURLConnection;

public class FormatGetHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FormatGetHandler.class);

    private final FormatController formatController = FormatController.getInstance();

    @Override
    public void handle(RoutingContext rCtx) {
        LOGGER.info("handle GET on {}", rCtx.request().path());
        FormatGetCommand formatGetCommand = new FormatGetCommand(rCtx.request().getParam("name"));

        getFormat(rCtx, formatGetCommand);
    }

    private void getFormat(RoutingContext rCtx, FormatGetCommand formatGetCommand) {
        Either.<Throwable, FormatGetCommand>right(formatGetCommand)
            .flatMap(formatController::get)
            .peekLeft(exception -> handleBadRequest(rCtx, exception))
            .peek(format -> handleSuccessfullGet(rCtx, format));
    }

    private void handleSuccessfullGet(RoutingContext rCtx, Format format) {
        LOGGER.info("successfully get format {}", format.getName());
        rCtx.response().end(format.toJsonObject().toString());
    }

    private void handleBadRequest(RoutingContext rCtx, Throwable exception) {
        int status = HttpURLConnection.HTTP_INTERNAL_ERROR;
        String message = "";

        if(exception instanceof FormatNotFoundException) {
            status = HttpURLConnection.HTTP_NOT_FOUND;
            message = exception.getMessage();
        }

        rCtx.response().setStatusCode(status).end(message);
    }
}
