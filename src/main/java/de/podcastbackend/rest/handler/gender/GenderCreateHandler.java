package de.podcastbackend.rest.handler.gender;

import de.podcastbackend.rest.commands.gender.GenderCreateCommand;
import de.podcastbackend.rest.controllers.GenderController;
import de.podcastbackend.rest.entities.Gender;
import de.podcastbackend.rest.exceptions.GenderFoundException;
import io.vavr.control.Either;
import io.vavr.control.Try;
import io.vertx.core.Handler;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import java.net.HttpURLConnection;

public class GenderCreateHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenderCreateHandler.class);

    private final GenderController genderController = GenderController.getInstance();

    @Override
    public void handle(RoutingContext rCtx) {
        LOGGER.info("handle post on " + rCtx.request().path());

        Either<Throwable, GenderCreateCommand> genderCreateCommands =
            decodeGenderCreateCommand(rCtx.getBodyAsString());

        if(genderCreateCommands.isLeft()) {
            handleBadRequest(rCtx, genderCreateCommands.getLeft());
        } else {
            createGenderAndStore(rCtx, genderCreateCommands.get());
        }
    }

    private void createGenderAndStore(RoutingContext rCtx, GenderCreateCommand genderCreateCommand) {
        Either.<Throwable, GenderCreateCommand>right(genderCreateCommand)
            .flatMap(genderController::create)
            .peekLeft(exception -> handleBadRequest(rCtx, exception))
            .peek(result -> handleSuccessfulCreation(rCtx, (Gender) result));
    }

    private void handleSuccessfulCreation(RoutingContext rCtx, Gender result) {
                LOGGER.info("successfully created gender");
                rCtx
                    .response()
                    .putHeader("Location", "/gender/" + result.getName())
                    .setStatusCode(201)
                    .end(result.toJsonObject().toString());
    }

    private void handleBadRequest(RoutingContext rCtx, Throwable exception) {

        int status = HttpURLConnection.HTTP_BAD_REQUEST;
        String message = "";

        if(exception instanceof GenderFoundException) {
            status = HttpURLConnection.HTTP_CONFLICT;
            message = exception.getMessage();
        }

        rCtx.response()
            .setStatusCode(status)
            .end(message);
    }

    private Either<Throwable, GenderCreateCommand> decodeGenderCreateCommand(String json) {
        LOGGER.info(String.format("JSON body %s", json ));
        return Try.of(() -> Json.decodeValue(json, GenderCreateCommand.class)).toEither();
    }
}
