package de.podcastbackend.rest.handler.gender;

import de.podcastbackend.rest.commands.gender.GenderGetCommand;
import de.podcastbackend.rest.controllers.GenderController;
import de.podcastbackend.rest.entities.Gender;
import de.podcastbackend.rest.exceptions.GenderNotFoundException;
import io.vavr.control.Either;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import java.net.HttpURLConnection;


public class GenderGetHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenderGetHandler.class);

    private final GenderController genderController = GenderController.getInstance();

    @Override
    public void handle(RoutingContext rCtx) {
        LOGGER.info("handle GET on {}", rCtx.request().path());
        GenderGetCommand genderGetCommand = new GenderGetCommand(rCtx.request().getParam("name"));

        getGender(rCtx, genderGetCommand);
    }

    public void getGender(RoutingContext rCtx, GenderGetCommand genderGetCommand) {
        Either.<Throwable, GenderGetCommand>right(genderGetCommand)
            .flatMap(genderController::get)
            .peekLeft(exception -> handleBadRequest(rCtx, exception))
            .peek(result -> handleSuccessfullGet(rCtx, result));
    }

    private void handleSuccessfullGet(RoutingContext rCtx, Gender result) {
        LOGGER.info("successfully get gender");
        rCtx.response().end(result.toJsonObject().toString());
    }

    private void handleBadRequest(RoutingContext rCtx, Throwable exception) {

        int status = HttpURLConnection.HTTP_INTERNAL_ERROR;
        String message = "";

        if(exception instanceof GenderNotFoundException) {
            status = HttpURLConnection.HTTP_NOT_FOUND;
            message = exception.getMessage();
        }

        rCtx.response()
            .setStatusCode(status)
            .end(message);
    }
}
