package de.podcastbackend.rest;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import de.podcastbackend.rest.AbstractRouteTest.RoutingVerticle;
import de.podcastbackend.rest.verticles.FormatVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension.class)
@RoutingVerticle(routingVerticleClass = FormatVerticle.class)
public class FormatRouteTest extends AbstractRouteTest {

    @Test
    @DisplayName("Creating Format")
    public void createFormat(Vertx vertx, VertxTestContext tCtx) {
        JsonObject body = new JsonObject()
            .put("name", "Borg ist nicht schwedisch")
            .put("description", "lorem ipsum")
            .put("logo", "https://example.com/logo.png");

        vertx.createHttpClient().post(PORT, "localhost", "/format")
            .handler(response -> {
                assertThat(response.statusCode(), equalTo(201));
                assertThat(response.getHeader("Location"), equalTo("/format/Borg ist nicht schwedisch"));
                tCtx.completeNow();
            })
            .setChunked(true)
            .write(body.encode())
            .end();
    }

    @Test
    @DisplayName("getting valid format")
    public void getFormat(Vertx vertx, VertxTestContext tCtx) {
        Future createFuture = Future.future();

        JsonObject body = new JsonObject()
            .put("name", "test")
            .put("description", "lorem ipsum")
            .put("logo", "https://example.com/logo.png");

        vertx.createHttpClient().post(PORT, "localhost", "/format")
            .handler(response -> {
                createFuture.complete();
            })
            .setChunked(true)
            .write(body.encode())
            .end();

        createFuture.setHandler(res -> {
            vertx.createHttpClient().get(PORT, "localhost", "/format/test")
                .handler(response -> {
                    assertThat(response.statusCode(), equalTo(HTTP_OK));
                    tCtx.completeNow();
                })
                .end();
        });
    }

    @Test
    @DisplayName("creating with invalid json")
    public void getWithInvalidJson(Vertx vertx, VertxTestContext tCtx) {
        vertx.createHttpClient().post(PORT, "localhost", "/format")
            .handler(response -> {
                assertThat(response.statusCode(), equalTo(HTTP_BAD_REQUEST));
                tCtx.completeNow();
            })
            .setChunked(true)
            .write("{\"name\": ")
            .end();
    }
}
