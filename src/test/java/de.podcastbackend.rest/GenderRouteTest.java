package de.podcastbackend.rest;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import de.podcastbackend.rest.AbstractRouteTest.RoutingVerticle;
import de.podcastbackend.rest.verticles.GenderVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension.class)
@RoutingVerticle(routingVerticleClass = GenderVerticle.class)
public class GenderRouteTest extends AbstractRouteTest {

    @Test
    @DisplayName("Creating Gender")
    public void createGender(Vertx vertx, VertxTestContext tctx) {
        JsonObject body = new JsonObject()
            .put("name", "female");

        vertx.createHttpClient().post(PORT, "localhost", "/gender")
            .handler(response -> {
                assertThat(response.statusCode(), equalTo(HTTP_CREATED));
                assertThat(response.getHeader("Location"), equalTo("/gender/female"));
                tctx.completeNow();
            })
            .setChunked(true)
            .write(body.encode())
            .end();
    }

    @Test
    @DisplayName("getting valid gender")
    public void getGender(Vertx vertx, VertxTestContext tCtx) {
        Future createFuture = Future.future();

        JsonObject body = new JsonObject()
            .put("name", "male");

        vertx.createHttpClient().post(PORT, "localhost", "/gender")
            .handler(response -> {
                createFuture.complete();
            })
            .setChunked(true)
            .write(body.encode())
            .end();

        createFuture.setHandler((res) -> {
            vertx.createHttpClient().get(PORT, "localhost", "/gender/male")
                .handler(response -> {
                    assertThat(response.statusCode(), equalTo(HTTP_OK));
                    tCtx.completeNow();
                })
                .end();
        });
    }

    @Test
    @DisplayName("getting unknown gender")
    public void getUnknownGender(Vertx vertx, VertxTestContext tCtx) {
        vertx.createHttpClient().get(PORT, "localhost", "/gender/helicopter")
            .handler(response -> {
                assertThat(response.statusCode(), equalTo(HTTP_NOT_FOUND));
                tCtx.completeNow();
            })
            .end();
    }

    @Test
    @DisplayName("creating with invalid json")
    public void getWithInvalidJson(Vertx vertx, VertxTestContext tCtx) {
        vertx.createHttpClient().post(PORT, "localhost", "/gender")
            .handler(response -> {
                assertThat(response.statusCode(), equalTo(HTTP_BAD_REQUEST));
                tCtx.completeNow();
            })
            .setChunked(true)
            .write("{\"name\":")
            .end();
    }
}
