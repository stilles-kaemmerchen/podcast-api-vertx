package de.podcastbackend.rest;

import de.podcastbackend.rest.verticles.MainVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.junit5.VertxTestContext;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;

public class AbstractRouteTest {

    public static final int PORT = 8080;

    @Documented
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    public @interface RoutingVerticle {
        Class routingVerticleClass();
    }

    @BeforeAll
    @DisplayName("deploying verticle")
    public void setUp(Vertx vertx, VertxTestContext tCtx)
        throws Exception {
        final Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());

        DeploymentOptions options = new DeploymentOptions()
            .setConfig(new JsonObject().put("http.port", PORT));

        Future verticleFuture = Future.future();
        Future mainFuture = Future.future();

        List<Future> futureList = new ArrayList();
        futureList.add(verticleFuture);
        futureList.add(mainFuture);

        CompositeFuture.all(futureList).setHandler( result -> {
            if(result.succeeded()) {
                tCtx.completeNow();
            }
        });

        if(!this.getClass().isAnnotationPresent(RoutingVerticle.class)) {
            throw new Exception("RoutingVerticle Annotation is missing");
        }

        Class routingVerticleClass = this.getClass()
            .getAnnotation(RoutingVerticle.class).routingVerticleClass();

        Constructor<?> constructor = routingVerticleClass.getConstructor(Router.class);
        AbstractVerticle verticle = (AbstractVerticle) constructor.newInstance(router);

        vertx.deployVerticle(verticle, deployment -> handleDeployment(verticleFuture, deployment));
        vertx.deployVerticle(new MainVerticle(router), options, deployment -> handleDeployment(mainFuture, deployment ));
    }

    private void handleDeployment(Future future, AsyncResult<String> deployment) {
        if(deployment.succeeded()) {
            future.complete(deployment.result());
        } else {
            future.fail(deployment.cause());
        }
    }

}
